from datetime import datetime
import os
from loguru import logger
from vk_parser import parser_info


def start():
    file = open("vk_groups.txt")
    with open("vk_groups.txt") as file:
        list_group = file.read().split(',')
        list_group = [group.rpartition('/')[-1] for group in list_group if group != '']
    parser_info(list_group)



if __name__ == "__main__":
    logger.add(
        "logs/{}".format(datetime.now().strftime("%d_%m_%Y.log")),
        format="{time} {level} {message}",
        rotation="10 MB",
        level="DEBUG",
        compression="zip")
    start()


