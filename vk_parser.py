import os
import time
from loguru import logger
import requests
from dotenv import dotenv_values, find_dotenv
import pandas as pd
import time
from progress.bar import IncrementalBar



settings = dotenv_values(find_dotenv())
URL = 'https://api.vk.com/method/groups.getMembers'
fields = "activities,about,blacklisted,blacklisted_by_me,books,bdate,can_be_invited_group,can_post,can_see_all_posts,can_see_audio,can_send_friend_request,can_write_private_message,connections,contacts,domain,exports,followers_count,friend_status,has_photo,has_mobile,home_town,sex,site,screen_name,status,verified,games,interests,is_favorite,is_friend,is_hidden_from_feed,maiden_name,movies,music,nickname,online,photo_id,photo_max,photo_max_orig,quotes,timezone,tv"


def get_offset(group_id):
    params = {
        'access_token': settings["TOKEN"],
        'group_id': group_id,
        'v': settings["VK_VERSION"]
    }

    r = requests.get(url=URL, params=params)
    count = r.json()['response']['count']
    logger.info(f'Количество подписчиков: {count}')
    if count > 1000:
        return (count // 1000) + 1
    else:
        count = 1
        return count
    

def get_users(group_id):
    all_users = []
    max_len = get_offset(group_id)

    bar = IncrementalBar(f'{group_id} - информация о подписчиках ', max = max_len)
    for offset in range(0, max_len):
        params = {
            'access_token': settings["TOKEN"], 
            'v': settings["VK_VERSION"], 
            'group_id': group_id, 
            'offset': offset*1000, 
            'fields': fields
        }
        time.sleep(0.2)
        users = requests.get(URL, params=params)
        if users.status_code != 125:
            users: dict = users.json()['response']["items"]
            all_users.extend(users)
        bar.next()

    df = pd.DataFrame(data=all_users)
    if not os.path.exists("files"):
        os.makedirs("files")
    print("\n")

    logger.info("Сейчас происходит сохранение файла, пожалуйста не прерывайте процесс")
    df.to_excel(f"files/{str(group_id)}.xlsx", sheet_name=str(group_id))

    logger.success(f"Было записано {len(all_users)} пользователей")
    bar.finish()
    return all_users


def parser_info(group_list: dict):
    group_list = {i: [] for i in group_list}

    for group in group_list.keys():
        logger.info(f'Группа: {group}')
        group_list[group] = get_users(group)






